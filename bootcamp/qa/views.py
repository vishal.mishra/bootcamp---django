from django.db.utils import IntegrityError
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.http import JsonResponse, Http404, HttpResponse
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.http import require_http_methods
from django.views.generic import CreateView, ListView, DetailView
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.views import View
from django.views.generic.edit import UpdateView
from celery.result import AsyncResult
import os


from bootcamp.helpers import ajax_required
from bootcamp.qa.models import Question, Answer
from bootcamp.qa.forms import QuestionForm, EditQuestionForm
from bootcamp.qa.tasks import download_task


class QuestionsIndexListView(LoginRequiredMixin, ListView):
    """CBV to render a list view with all the registered questions."""
    model = Question
    paginate_by = 20
    context_object_name = "questions"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["popular_tags"] = Question.objects.get_counted_tags()
        context["active"] = "all"
        return context


class QuestionAnsListView(QuestionsIndexListView):
    """CBV to render a list view with all question which have been already
    marked as answered."""
    def get_queryset(self, **kwargs):
        return Question.objects.get_answered()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["active"] = "answered"
        return context


class QuestionListView(QuestionsIndexListView):
    """CBV to render a list view with all question which haven't been marked
    as answered."""
    def get_queryset(self, **kwargs):
        return Question.objects.get_unanswered()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["active"] = "unanswered"
        return context


class QuestionDetailView(LoginRequiredMixin, DetailView):
    """View to call a given Question object and to render all the details about
    that Question."""
    model = Question
    context_object_name = "question"


class CreateQuestionView(LoginRequiredMixin, CreateView):
    """
    View to handle the creation of a new question
    """
    form_class = QuestionForm
    template_name = "qa/question_form.html"
    message = _("Your question has been created.")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, self.message)
        return reverse("qa:index_noans")


class EditQuestionView(LoginRequiredMixin, UpdateView):
    """
    View to Edit the Question and Create an Updated one
    Question.objects.get(pk=1)[0].title
    """
    model = Question
    form_class = EditQuestionForm
    template_name = "qa/edit_question_form.html"
    message = _("Your question has been Updated!.")

    def form_valid(self, form):
        if form.instance.user == self.request.user:
            return super().form_valid(form)
        else:
            return Http404()

    def get_success_url(self):
        messages.success(self.request, self.message)
        return reverse("qa:index_noans")


class CreateAnswerView(LoginRequiredMixin, CreateView):
    """
    View to create new answers for a given question
    """
    model = Answer
    fields = ["content", ]
    message = _("Thank you! Your answer has been posted.")

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.question_id = self.kwargs["question_id"]
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, self.message)
        return reverse(
            "qa:question_detail", kwargs={"pk": self.kwargs['question_id']})


class EditAnswerView(LoginRequiredMixin, UpdateView):
    """
    View to create new answers for a given question
    """
    model = Answer
    template_name = 'qa/edit_answer_form.html'
    fields = ["content", ]
    message = _("Thank you! Your answer has been updated.")

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.uuid_id = self.kwargs["pk"]
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, self.message)
        answer = Answer.objects.get(pk=self.kwargs['pk'])
        return reverse(
            "qa:question_detail", kwargs={"pk": answer.question_id})


def DeleteAnswerView(request, pk):
    answer = get_object_or_404(Answer, uuid_id=pk)
    answer.delete()
    return HttpResponseRedirect(reverse(
        "qa:question_detail", kwargs={"pk": answer.question_id}))


class DownloadQuestionView(LoginRequiredMixin, View):

    def get(self, request):
        return render(request, 'qa/download_page.html')

    def post(self, request, user_id):
        task = download_task.delay(user_id)
        return JsonResponse({'task_id': task.id, 'status': task.status})


@login_required
@ajax_required
def download_status(request):
    status = AsyncResult(request.GET.get('task_id'))
    return JsonResponse({'status': status.state})


@login_required
def downloadfiles(request, user_id):
    try:
        filename = str(user_id) + '.json'
        file_path = os.path.join(os.environ.get('HOME'), filename)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fp:
                response = HttpResponse(fp.read(),
                                        content_type="application/json",)
                response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
            return response
    except:
        return Http404


@login_required
@ajax_required
@require_http_methods(["POST"])
def question_vote(request):
    """Function view to receive AJAX call, returns the count of votes a given
    question has recieved."""
    question_id = request.POST["question"]
    value = None
    if request.POST["value"] == "U":
        value = True

    else:
        value = False

    question = Question.objects.get(pk=question_id)
    try:
        question.votes.update_or_create(
            user=request.user, defaults={"value": value}, )
        question.count_votes()
        return JsonResponse({"votes": question.total_votes})

    except IntegrityError:  # pragma: no cover
        return JsonResponse({'status': 'false',
                             'message': _("Database integrity error.")},
                            status=500)


@login_required
@ajax_required
@require_http_methods(["POST"])
def answer_vote(request):
    """Function view to receive AJAX call, returns the count of votes a given
    answer has recieved."""
    answer_id = request.POST["answer"]
    value = None
    if request.POST["value"] == "U":
        value = True

    else:
        value = False

    answer = Answer.objects.get(uuid_id=answer_id)
    try:
        answer.votes.update_or_create(
            user=request.user, defaults={"value": value}, )
        answer.count_votes()
        return JsonResponse({"votes": answer.total_votes})

    except IntegrityError:  # pragma: no cover
        return JsonResponse({'status': 'false',
                             'message': _("Database integrity error.")},
                            status=500)


@login_required
@ajax_required
@require_http_methods(["POST"])
def accept_answer(request):
    """Function view to receive AJAX call, marks as accepted a given answer for
    an also provided question."""
    answer_id = request.POST["answer"]
    answer = Answer.objects.get(uuid_id=answer_id)
    answer.accept_answer()
    return JsonResponse({'status': 'true'}, status=200)
