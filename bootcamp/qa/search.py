from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Search, Object
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from . import models


connections.create_connection(hosts=['https://elastic:SwIMe9CpTcCLpbIwjtrCZ2PJ@cec5533506d24e7bb63d343fcd9c7de5.sa-east-1.aws.found.io:9243'], timeout=20)


class QuestionIndex(DocType):
    """Indexing the Question model"""
    user = Object(dynamic=True)
    title = Text()
    timestamp = Date()
    content = Text()
    pk = Text()

    class Index:
        name = 'question_index'


# Bulk indexing function, to be ran in shell
def bulk_indexing():
    es = Elasticsearch()
    for i in models.Question.objects.all():
        i.indexing()

# Simple search function
def search(query):
    s = Search().query("query_string", query='*' + query + '*',
                       fields=['title', 'content', 'user.username'])
    response = s.execute()
    return response
