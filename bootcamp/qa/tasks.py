from django.http import JsonResponse
from bootcamp.celery import app
from bootcamp.qa.models import Question
import json,os


@app.task
def download_task(user_id):
    user_questions = Question.objects.filter(user=user_id)
    user_data = {}
    for el in user_questions:
        content = {'title': el.title,
                   'timestamp': el.timestamp.isoformat(),
                   'content': el.content
                   }
        user_data[el.id] = (content)
    filepath = os.path.join(os.environ.get('HOME'),
                            str(user_id) + '.json')
    with open(filepath, 'w') as fp:
        json.dump(user_data, fp)

'''
user_data = json.dumps(user_data)
r.set('user_data', user_data)
    user_data = json.dumps(user_data, indent=4,
                          sort_keys=True, default=str)
    user_data = json.loads(data)
    return JsonResponse(user_data, safe=False)
'''
