# Use an official Python runtime as a parent image
FROM python:3.6.7-slim

# envn variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


# Copy the current directory contents into the container at /bootcamp
COPY . /bootcamp
WORKDIR /bootcamp
# Installing other packages
#RUN apt-get install -y redis-server postgresql postgresql-contrib


# Install any needed packages specified in requirements.txt
RUN pip install --upgrade pip 
RUN apt-get update
RUN apt-get install -y libpq-dev python3-dev build-essential redis-server postgresql-server-dev-all
RUN pip install -r /bootcamp/requirements.txt gunicorn==19.8.1

# Make port 80 available to the world outside this container
EXPOSE 8000

# Define environment variable
ENV NAME bootcamp

# Run app.py when the container launches


